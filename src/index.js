import React from 'react';
import ReactDOM from 'react-dom';
import { Router as ReactRouter, Route } from 'react-router-dom';
import './index.css';
import Router from './Router';
import history from './History';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <ReactRouter history={history}>
    <Route path="/" component={Router} />
  </ReactRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
