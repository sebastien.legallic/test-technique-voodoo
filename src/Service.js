import axios from 'axios';
import history from './History';
import { BASE_URL_A, BASE_URL_M, API_KEY_M } from './Constantes';

const catchMyError = error => {
  console.error('[Error] : ', error);
  if (error && error.response) {
    if (error.response.status === 404) {
      history.replace('/404');
    } else if (error.response.status === 500) {
      history.replace('/500');
    }
  }
};

export const getAcquisitionData = async params => {
  return axios.get(`${BASE_URL_A}`, { params }).catch(catchMyError);
};

export const getMonetizationData = async params => {
  return axios
    .get(`https://cors-anywhere.herokuapp.com/${BASE_URL_M}`, {
      params,
      headers: { Authorization: 'Bearer ' + API_KEY_M }
    })
    .catch(catchMyError);
};
