export const BASE_URL_A = 'http://mock-api.voodoo.io/acquisition';
export const BASE_URL_M = 'http://mock-api.voodoo.io/monetization';
export const API_KEY_A =
  '31I4HHdML8AH30OQCqbuRswzFcvhigvs3f15UQqc6VuOnTNzKYJosB43I5vE2o2SmwNYhh7oCS5X1XUJjhDzlnX9RugHJ';
export const API_KEY_M =
  'mwNNiwFuJ30GqpuYwQHSW0XQx93E2rIS7NRSfxwLz4XI5Yoo5aSP8wvyibhVO8aYeaVLYsCJcFP9V0uzo95ph66qktQwE';
export const SERVER_FORMAT = 'YYYY-MM-DD';
export const COLUMNS = [
  { id: 'country', libelle: 'Country' },
  { id: 'platform', libelle: 'Platform' },
  { id: 'cost', libelle: 'Cost' },
  { id: 'application', libelle: 'Application' },
  { id: 'ad_type', libelle: 'Ad Type' }
];
export const MAP = {
  platform: 'os',
  application: 'game',
  ad_type: 'format'
};

export const IS_MOCK = false;
