import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Home from './Home/Home';

const Router = props => {
  const render404 = () => () => {
    return <h1> OOOooops 404 ! </h1>;
  };

  const render500 = () => () => {
    return <h1> Erreur 500 ! Petit problème avec le serveur ! </h1>;
  };

  let base = 'Voodoo | ';
  switch (props.location.pathname.split('/')[1]) {
    case '':
      document.title = base + 'Home';
      break;
    default:
      document.title = '| ' + base;
  }

  return (
    <>
      <Switch>
        <Route path="/404" component={render404()} />
        <Route path="/500" component={render500()} />
        <Route path="/" exact={true} render={props => <Home {...props} />} />
        <Redirect to="/404" />
      </Switch>
    </>
  );
};

export default Router;
