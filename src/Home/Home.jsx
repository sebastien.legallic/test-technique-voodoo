import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import CircularProgress from '@material-ui/core/CircularProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker } from 'react-dates';
import style from './Home.module.css';
import MockAcquisition from '../Mocks/MockAcquisition';
import MockMonetization from '../Mocks/MockMonetization';

import { getAcquisitionData, getMonetizationData } from '../Service';
import SelectFilter from './SelectFilter';
import { API_KEY_A, SERVER_FORMAT, COLUMNS, MAP, IS_MOCK } from '../Constantes';

const Home = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isAcquisitionDone, setIsAcquisitionDone] = useState(false);
  const [isMonetization, setIsMonetization] = useState(false);

  const [focus, setFocus] = useState(null);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const [platforms, setPlatforms] = useState([]);
  const [selectedPlatforms, setSelectedPlatforms] = useState([]);

  const [selectedCountries, setSelectedCountries] = useState([]);
  const [countries, setCountries] = useState([]);

  const [apps, setApps] = useState([]);
  const [selectedApps, setSelectedApps] = useState([]);

  const [adType, setAdType] = useState([]);
  const [selectedAdType, setSelectedAdType] = useState([]);

  const [dimensions, setDimensions] = useState([]);
  const [monetization, setMonetization] = useState([]);
  const [allCosts, setAllCosts] = useState([]);
  const [totalCost, setTotalCost] = useState(0);

  const onDateChange = dates => {
    setStartDate(dates.startDate);
    setEndDate(dates.endDate);
  };

  const round = nb => {
    return Math.round(nb * 100) / 100;
  };

  const getAcquisition = async () => {
    if (endDate && startDate) {
      if (IS_MOCK) {
        processAcquisitionData(MockAcquisition.data);
      } else {
        setIsLoading(true);
        const params = {
          api_key: API_KEY_A,
          start: startDate.format(SERVER_FORMAT),
          end: endDate.format(SERVER_FORMAT),
          format: 'json',
          columns: COLUMNS.map(col => col.id).join(',')
        };
        getAcquisitionData(params).then(r => {
          if (r && r.data && r.data.data) {
            processAcquisitionData(r.data.data);
          } else {
            console.error(r);
          }
        });
      }
    }
  };

  const processAcquisitionData = datas => {
    console.log(datas);
    let nextCountries = [];
    let nextApps = [];
    let nextPlatforms = [];
    let nextAdType = [];
    let nextAllCost = [];
    datas.forEach(({ application, platform, ad_type, country, cost }) => {
      if (!nextPlatforms.includes(platform)) {
        nextPlatforms.push(platform);
      }
      if (!nextCountries.includes(country)) {
        nextCountries.push(country);
      }
      if (!nextApps.includes(application)) {
        nextApps.push(application);
      }
      if (!nextAdType.includes(ad_type)) {
        nextAdType.push(ad_type);
      }
      nextAllCost[country] = nextAllCost[country] ? nextAllCost[country] + cost : cost;
    });
    setAllCosts(nextAllCost);
    setPlatforms(nextPlatforms);
    setCountries(nextCountries);
    setApps(nextApps);
    setAdType(nextAdType);
    setIsLoading(false);
    setIsAcquisitionDone(true);
  };

  const getMonetization = () => {
    const uniqDimensions = [...new Set(dimensions.map(dim => dim.dimensionType))];
    const params = {
      start: startDate.format(SERVER_FORMAT),
      end: endDate.format(SERVER_FORMAT),
      dimensions: 'country,' + uniqDimensions.join(','),
      aggregates: 'revenue'
    };
    if (IS_MOCK) {
      processMonetizationData(MockMonetization.data);
    } else {
      setIsLoading(true);
      getMonetizationData(params).then(r => {
        if (r && r.data && r.data.data) {
          processMonetizationData(r.data.data);
        } else {
          console.error(r);
        }
      });
    }
  };

  const processMonetizationData = datas => {
    setMonetization(datas);
    setIsMonetization(true);
    setIsLoading(false);
  };

  useEffect(() => {
    let nextDimensions = [];
    selectedPlatforms.forEach(p => nextDimensions.push({ dimensionType: MAP['platform'], dimension: p }));
    selectedApps.forEach(app => nextDimensions.push({ dimensionType: MAP['application'], dimension: app }));
    selectedAdType.forEach(ad => nextDimensions.push({ dimensionType: MAP['ad_type'], dimension: ad }));

    setDimensions(nextDimensions);
  }, [selectedAdType, selectedApps, selectedPlatforms]);

  useEffect(() => {
    let nextTotal = 0;
    selectedCountries.forEach(country => (nextTotal += allCosts[country]));
    setTotalCost(nextTotal);
  }, [allCosts, selectedCountries]);

  return (
    <div className={style.fullSize}>
      <div className={style.someSpace}>
        <DateRangePicker
          startDate={startDate}
          startDateId="startDate"
          endDate={endDate}
          endDateId="endDate"
          onDatesChange={onDateChange}
          focusedInput={focus}
          onFocusChange={focusedInput => setFocus(focusedInput)}
          isOutsideRange={() => false}
          numberOfMonths={1}
          showClearDates
          openDirection="up"
        />
        <Button variant="contained" className={style.button} onClick={getAcquisition} disabled={isLoading}>
          {isLoading ? (
            <CircularProgress />
          ) : (
            <>
              Acquisition <SendIcon />
            </>
          )}
        </Button>
      </div>
      {isAcquisitionDone && (
        <>
          <div className={style.filters}>
            <SelectFilter
              libelle={'Countries'}
              options={countries}
              value={selectedCountries}
              onChange={event => setSelectedCountries(event.target.value)}
            />
            <SelectFilter
              libelle={'Platforms'}
              options={platforms}
              value={selectedPlatforms}
              onChange={event => setSelectedPlatforms(event.target.value)}
            />
            <SelectFilter
              libelle={'Applications'}
              options={apps}
              value={selectedApps}
              onChange={event => setSelectedApps(event.target.value)}
            />
            <SelectFilter
              libelle={'Ad types'}
              options={adType}
              value={selectedAdType}
              onChange={event => setSelectedAdType(event.target.value)}
            />
            <Button variant="contained" className={style.button} onClick={getMonetization} disabled={isLoading}>
              {isLoading ? (
                <CircularProgress />
              ) : (
                <>
                  Monetization <SendIcon />
                </>
              )}
            </Button>
          </div>
          {isMonetization && (
            <div className={style.someSpace}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell>Dimensions</TableCell>
                    <TableCell />
                    {selectedCountries.map((country, i) => (
                      <TableCell key={i}>{country}</TableCell>
                    ))}
                    <TableCell>Totals</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow hover role="checkbox" tabIndex={-1} key={`costs`} className={style.costs}>
                    <TableCell>Costs</TableCell>
                    <TableCell></TableCell>
                    {selectedCountries.map((country, k) => {
                      return <TableCell key={k}>{round(allCosts[country])}</TableCell>;
                    })}
                    <TableCell>
                      <span className={style.bold}>{round(totalCost)}</span>
                    </TableCell>
                  </TableRow>
                  {dimensions.map(({ dimensionType, dimension }, i) => {
                    let totalCol = 0;
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={`${dimension}${i}`}>
                        <TableCell>{dimensionType}</TableCell>
                        <TableCell>{dimension}</TableCell>
                        {selectedCountries.map((country, j) => {
                          const data = monetization.filter(
                            m => m.country === country && m[dimensionType] === dimension
                          );
                          let value = 0;
                          data && data.forEach(line => (value += line.revenue));
                          totalCol += value;
                          return <TableCell key={j}>{round(value)}</TableCell>;
                        })}
                        <TableCell>
                          <span className={style.bold}>{round(totalCol)}</span>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default Home;
