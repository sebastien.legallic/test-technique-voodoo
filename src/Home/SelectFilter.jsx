import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import style from './SelectFilter.module.css';

const SelectFilter = ({ options, value = [], libelle, onChange }) => {
  return (
    <FormControl className={style.formControl}>
      <InputLabel htmlFor="select-multiple-checkbox">{libelle}</InputLabel>
      <Select
        multiple
        className={style.select}
        value={value}
        onChange={onChange}
        input={<Input id="select-multiple-checkbox" />}
        renderValue={() => value && value.join(', ')}
        disabled={options && options.length === 0}
      >
        {options.map((opt, i) => (
          <MenuItem key={i} value={opt}>
            <Checkbox checked={value && value.includes(opt)} />
            <ListItemText primary={opt} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SelectFilter;
